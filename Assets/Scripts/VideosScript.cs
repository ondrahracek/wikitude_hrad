﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideosScript : MonoBehaviour
{
    public VideoPlayer myVideoPlayer;

    public void VideoPlay(int index)
    {
        myVideoPlayer.Play();
    }
    public void VideoStop(int index)
    {
        myVideoPlayer.Stop();
    }
    public void VideoPausePlay()
    {
        if (!myVideoPlayer.isPaused)
            myVideoPlayer.Pause();
        else
            myVideoPlayer.Play();
    }
    public void VideoMute()
    {
        myVideoPlayer.SetDirectAudioMute(0, !myVideoPlayer.GetDirectAudioMute(0));
    }

    public void LoadVideoScene()
    {
        SceneManager.LoadScene("videos");
    }


    public void LoadMainLevel()
    {
        SceneManager.LoadScene("default");
    }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
