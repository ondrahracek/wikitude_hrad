﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WebOnClickScript : MonoBehaviour
{
    public string adress = "https://www.b4l.cz/cs/";

    private void OnMouseDown()
    {
        Application.OpenURL(adress);
    }

}
