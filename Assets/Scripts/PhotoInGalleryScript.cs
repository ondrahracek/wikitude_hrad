﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoInGalleryScript : MonoBehaviour
{

    public bool selected;

    public GameObject selectedIndicator;

    public string myFile;

    public void Clicked()
    {
        selected =!selected;
        selectedIndicator.SetActive(selected);
    }

}
