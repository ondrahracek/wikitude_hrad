﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestingInfoScript : MonoBehaviour
{

    public Text infoText;

    public Transform camera;

    public Transform tracker;
    public Transform trackable;

    public Transform castle;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Vector3.one.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        string info = "";
        info += "Camera pos: " + camera.transform.position.ToString();
        info += "\nCamera rot: " + camera.transform.eulerAngles.ToString();

        info += "\nTracker pos: " + tracker.transform.position.ToString();
        info += "\nTracker rot: " + tracker.transform.eulerAngles.ToString();

        info += "\nTrackable pos: " + trackable.transform.position.ToString();
        info += "\nTrackable rot: " + trackable.transform.eulerAngles.ToString();

        info += "\nCastle pos: " + castle.transform.position.ToString();
        info += "\nCastle rot: " + castle.transform.eulerAngles.ToString();

        infoText.text = info;
    }
}
