﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChildModelsScript : MonoBehaviour
{

    public GameObject[] myObjects;
    public int active = 0;
    int lastActive = -1;


    // Start is called before the first frame update
    void Start()
    {
        //lastActive = active;
        foreach (GameObject g in myObjects)
            g.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (myObjects.Length > 0)
        {
            active = active % myObjects.Length;

            if (lastActive != active)
            {
                if (lastActive != -1)
                {
                    myObjects[lastActive].SetActive(false);
                }
                myObjects[active].SetActive(true);
                lastActive = active;
            }
        }
    }
}
