﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wikitude;

public class ExtendedTrackingLostScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTrackableLost()
    {
        this.GetComponentInParent<ImageTracker>().StopExtendedTracking();
    }
}
