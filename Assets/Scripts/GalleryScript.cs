﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class GalleryScript : MonoBehaviour
{
    public GameObject galleryBtn;

    public GameObject photoPrefab;
    public Transform photosParent;
    public RectTransform photosParentRectTransform;
    public List<string> photos;
    public Button shareBtn;
    public GameObject loadingImgObject;

    List<string> instantiatedPhotos;
    List<GameObject> instantiatedPrefabs;
    List<PhotoInGalleryScript> instantiatedPhotoInGallerys;


    List<string> selectedPhotos;

    private void Awake()
    {
        instantiatedPhotos = new List<string>();
        instantiatedPrefabs = new List<GameObject>();
        instantiatedPhotoInGallerys = new List<PhotoInGalleryScript>();
    }

    public void OpenGallery()
    {
        this.gameObject.SetActive(true);
        galleryBtn.SetActive(false);
        LoadGallery();
    }

    IEnumerator LoadImages()
    {
        for (int i = 0; i < photos.Count; i++)
        {
            if (!instantiatedPhotos.Contains(photos[i]))
            {
                try
                {
                    GameObject b = GameObject.Instantiate(photoPrefab, photosParent);
                    byte[] bytes = File.ReadAllBytes(photos[i]);
                    Texture2D tex = new Texture2D(/*Screen.width/10, Screen.height/10*/2, 2, TextureFormat.RGB24, false);
                    tex.filterMode = FilterMode.Trilinear;
                    tex.LoadImage(bytes);
                    Sprite sprite = Sprite.Create(tex, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0.5f, 0.5f/*Screen.width / 8, Screen.height / 8*/));

                    Image img = b.GetComponent<Image>();
                    img.sprite = sprite;

                    instantiatedPhotos.Add(photos[i]);
                    instantiatedPrefabs.Add(b);
                    RectTransform t = b.GetComponent<RectTransform>();
                    t.localPosition = new Vector3(((i % 3) - 1) * 112, ((int)((float)i / 3f)) * -112 - 56 + 275, 0);
                    PhotoInGalleryScript s = b.GetComponent<PhotoInGalleryScript>();
                    s.myFile = photos[i];
                    instantiatedPhotoInGallerys.Add(s);
                }
                catch
                {

                }
            }
        }
        photosParentRectTransform.sizeDelta = new Vector2(112 * 3, (photos.Count / 3 + 1) * 112);

        yield return null;
    }

    public void LoadGallery()
    {
        StartCoroutine(LoadGalleryC());
    }

    public IEnumerator LoadGalleryC()
    {
        loadingImgObject.SetActive(true);

        string path = Application.persistentDataPath;

        photos = new List<string>();
        string[] imgs = Directory.GetFiles(path, "*.jpeg", SearchOption.TopDirectoryOnly);
        photos.AddRange(imgs);
        Debug.Log("Num of images ad " + path + ": " + imgs.Length);

        if (false)
        {
            foreach (GameObject g in instantiatedPrefabs)
                Destroy(g);
            instantiatedPrefabs.Clear();
            instantiatedPhotos.Clear();
            instantiatedPhotoInGallerys.Clear();
        }


        yield return StartCoroutine(LoadImages());

        Debug.Log("Done!");
        loadingImgObject.SetActive(false);
    }

    private void Update()
    {
        selectedPhotos = new List<string>();
        foreach (PhotoInGalleryScript s in instantiatedPhotoInGallerys)
        {
            if (s.selected)
                selectedPhotos.Add(s.myFile);
        }
        if (selectedPhotos.Count <= 0)
        {
            shareBtn.interactable = (false);
        }
        else
        {
            shareBtn.interactable = (true);
        }
    }


    public void Share()
    {
        selectedPhotos = new List<string>();
        foreach (PhotoInGalleryScript s in instantiatedPhotoInGallerys)
        {
            if (s.selected)
                selectedPhotos.Add(s.myFile);
        }
        if (selectedPhotos.Count <= 0)
            return;



        ////PRO IOS MUSIM JIT PODLE INSTRUKCI Assets/Plugins/NativeShare/README.txt
        NativeShare ns = new NativeShare();
        ns.SetSubject("AR Hrad");
        ns.SetText("Posílám fotky.");
        foreach (string file in selectedPhotos)
            ns.AddFile(file);
        ns.SetTitle("AR Hrad");
        ns.Share();
    }


    public void CloseGallery()
    {
        galleryBtn.SetActive(true);
        gameObject.SetActive(false);
    }

}
