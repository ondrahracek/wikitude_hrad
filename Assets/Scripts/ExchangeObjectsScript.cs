﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExchangeObjectsScript : MonoBehaviour
{

    public GameObject myEnablingObject;

    public bool canDisableAll;
    public GameObject[] objects;
    public RectTransform[] buttons;

    public int selected = -1;

    

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].anchoredPosition =
                Vector2.Lerp(buttons[i].anchoredPosition,
                selected == i ? new Vector2(-50, buttons[i].anchoredPosition.y) : new Vector2(-36, buttons[i].anchoredPosition.y),
                Time.deltaTime * 5f);
            if (myEnablingObject)
                buttons[i].gameObject.SetActive(myEnablingObject.activeSelf);
        }
    }

    public void ObjectSelected(int n)
    {
        if (!canDisableAll)
            selected = n;
        else
            selected = selected == n ? -1 : n;
        for (int i = 0; i < objects.Length; i++)
        {
            bool isParticle = false;
            foreach (ParticleSystem p in objects[i].GetComponentsInChildren<ParticleSystem>())
            {
                isParticle = true;
                Debug.Log("Particle!");
                if (i == selected)
                {
                    Debug.Log("Playing!");
                    p.Play();
                }
                else
                {
                    Debug.Log("Stopping!" + i);
                    p.Stop(true, ParticleSystemStopBehavior.StopEmitting);
                }
            }
            if(!isParticle)
            {
                Debug.Log("Not a particle!");
                objects[i].SetActive(i == selected);
            }
        }
    }
}
