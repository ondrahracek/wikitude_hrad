﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Drawing;
using System.IO;
using UnityEngine.UI;

public class ScreenshotterScript : MonoBehaviour
{
    List<string> screens;
    string nowFile = "";

    public Image myPhotoImage;
    public GameObject ramCanvas;
    public GameObject normalCanvas;

    [SerializeField]
    private GameObject _gdprCanvas;

    [SerializeField]
    private GameObject _photoBtn;

    // Start is called before the first frame update
    void Start()
    {
        screens = new List<string>();
        Debug.Log(Application.dataPath);
    }

    public void ShareButtonClicked(string file)
    {
        ////PRO IOS MUSIM JIT PODLE INSTRUKCI Assets/Plugins/NativeShare/README.txt
        NativeShare ns = new NativeShare();
        ns.SetSubject("AR Hrad");
        ns.SetText("Posílám fotky.");
        //ns.AddFile(file);
        ns.SetTitle("AR Hrad");
        ns.Share();
    }

    public void SharePhoto()
    {
        _gdprCanvas.SetActive(true);
        _photoBtn.SetActive(false);
    }

    public void AcceptGDPR(bool accepted)
    {
        _gdprCanvas.SetActive(false);
        _photoBtn.SetActive(true);

        if (accepted)
        {
            if (string.IsNullOrEmpty(nowFile))
                return;

            NativeShare ns = new NativeShare();
            ns.SetSubject("Hrad Zborov");
            ns.SetText("Ďakujeme za využitie našich služieb a poslaním tohto emailu potvrdzujete, že ste súhlasili so všeobecnými podmienkamia spracovaním osobných udajov odškrtnutím políčka při používaní aplikácie...");
            ns.AddFile(nowFile);
            ns.SetTitle("Hrad Zborov");
            ns.Share();
        }
    }

    public void ShowPhoto()
    {

        if (normalCanvas)
            normalCanvas.SetActive(true);
        if (ramCanvas)
            ramCanvas.SetActive(false);
        if (!myPhotoImage) return;
        try
        {
            byte[] bytes = File.ReadAllBytes(nowFile);
            Texture2D tex = new Texture2D(/*Screen.width/10, Screen.height/10*/2, 2, TextureFormat.RGB24, false);
            tex.filterMode = FilterMode.Trilinear;
            tex.LoadImage(bytes);
            //tex.Resize(tex.width / 2, tex.height / 2);
            float ySpriteSize = Screen.height / 2;
            Sprite sprite = Sprite.Create(tex, new Rect(0, 0, /*ySpriteSize / tex.height * tex.width, ySpriteSize*/Screen.width, Screen.height), new Vector2(0.5f, 0.5f/*Screen.width / 8, Screen.height / 8*/));

            myPhotoImage.sprite = sprite;
            myPhotoImage.gameObject.SetActive(true);
        }
        catch
        {

        }
    }

    public void TakeScreenshotAndDisplayIt()
    {
        // Osetreni aby se vypnulo okno
        _gdprCanvas.SetActive(false);

        if (normalCanvas)
            normalCanvas.SetActive(false);
        if (ramCanvas)
            ramCanvas.SetActive(true);

        string name = /*ziskej jmeno objektu, se kterym se foti + */ DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss_") + DateTime.Now.Millisecond.ToString() + ".png";
#if UNITY_EDITOR
        name = Application.dataPath + "/" + name;
#endif
        //#if !UNITY_EDITOR
        //        NativeToolkit.SaveScreenshot(name, "AR Hrady");
        //#endif
        ScreenCapture.CaptureScreenshot(name);
#if !UNITY_EDITOR
        string path = Application.persistentDataPath + "/" + name;
#else
        string path = name;
#endif
        //screens.Add(path);

        nowFile = path;

        //VVVVV    tuhle píčovinu nahradit Update(){ CheckForFile(file) } //DONE
        //Invoke("ShowPhoto", 2f);
        lookingForPhoto = true;
    }

    bool lookingForPhoto = false;
    // Update is called once per frame
    void Update()
    {
        if (lookingForPhoto)
        {
            if (File.Exists(nowFile))
            {
                lookingForPhoto = false;
                ShowPhoto();
            }
        }
    }

    public void SendScreenshotsToMail()
    {
        for (int i = 0; i < screens.Count; i++)
        {
            Debug.Log(screens[i]);
            if (!File.Exists(screens[i]))
            {
                screens.RemoveAt(i);
                i--;
            }
        }

        if (screens.Count > 0)
        {
            ShareButtonClicked(screens[0]);
            return;
        }

        string[] scrs = new string[screens.Count];
        for (int i = 0; i < scrs.Length; i++)
        {
            scrs[i] = screens[i];
        }
        string mailBody = "<p>Ahoj, posílám Ti e-mail s fotkama.</p>" +
            "<p>Více se dozvíš na webu <a href=\"http://tastyair.io/\">Tasty Air</a>.</p>";
        ManagerScript.SendEmail(mailBody, "Test posílání mailů", /*"ondra.hracek@seznam.cz"*/"matej.rejnoch@gmail.com", scrs);
    }

    public void CaptureScreenshot()
    {
        //if (!Directory.Exists("/storage/emulated/0/Pictures/AR Hrad"))
        //{
        //    Directory.CreateDirectory("/storage/emulated/0/Pictures/AR Hrad");
        //}

        string name = /*ziskej jmeno objektu, se kterym se foti + */ DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss_") + DateTime.Now.Millisecond.ToString() + ".jpeg";
        //#if !UNITY_EDITOR
        //        NativeToolkit.SaveScreenshot(name, "AR Hrady");
        //#endif
        ScreenCapture.CaptureScreenshot(name);
        screens.Add(Application.persistentDataPath + "/" + name);
        return;


        string myFileName = "Screenshot" + System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".png";
        ScreenCapture.CaptureScreenshot(myFileName);
        string myScreenshotLocation = Application.persistentDataPath + "/" + myFileName;


        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
        AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + myScreenshotLocation) });
        objActivity.Call("sendBroadcast", objIntent);
        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE

        return;

        /*string*/ name = /*ziskej jmeno objektu, se kterym se foti + */ DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss_") + DateTime.Now.Millisecond.ToString() + ".png";
        //#if UNITY_EDITOR
        Debug.Log(name);
        //#else
        RenderTexture renderTexture = Camera.main.targetTexture;

        Texture2D tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);//ScreenCapture.CaptureScreenshotAsTexture(/*@"AR Hrady\" + */);
        Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
        tex.ReadPixels(rect, 0, 0);
        Byte[] bytes = tex.EncodeToPNG();
        FileStream f = File.Open(Application.persistentDataPath + "/AR Hrad/" + name, FileMode.Create);
        BinaryWriter binary = new BinaryWriter(f);
        binary.Write(bytes);
        f.Close();
        RenderTexture.ReleaseTemporary(renderTexture);
        //#endif


    }

    IEnumerator CRSaveScreenshot()
    {
        yield return new WaitForEndOfFrame();
        // string TwoStepScreenshotPath = MobileNativeShare.SaveScreenshot("Screenshot" + System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second);
        // Debug.Log("A new screenshot was saved at " + TwoStepScreenshotPath);

        string myFileName = "Screenshot" + System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".png";
        string myDefaultLocation = Application.persistentDataPath + "/" + myFileName;
        string myFolderLocation = "/storage/emulated/0/DCIM/AR Hrad/";  //EXAMPLE OF DIRECTLY ACCESSING A CUSTOM FOLDER OF THE GALLERY
        string myScreenshotLocation = myFolderLocation + myFileName;

        //ENSURE THAT FOLDER LOCATION EXISTS
        if (!System.IO.Directory.Exists(myFolderLocation))
        {
            System.IO.Directory.CreateDirectory(myFolderLocation);
        }

        ScreenCapture.CaptureScreenshot(myFileName);
        //MOVE THE SCREENSHOT WHERE WE WANT IT TO BE STORED

        yield return new WaitForSeconds(1);

        System.IO.File.Move(myDefaultLocation, myScreenshotLocation);

        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
        AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + myScreenshotLocation) });
        objActivity.Call("sendBroadcast", objIntent);
        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE
    }
}
