﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
using UnityEngine.SceneManagement;
using System.Net.Mail;
using System.Net;

public class ManagerScript : MonoBehaviour
{
    public GameObject cameraWarning;

    public GalleryScript gallery;

    public GameObject mainScreenObject;
    public GameObject infoPanelObject;

    public GameObject popupObject;

    [SerializeField]
    private GameObject _photoBtn;

    [SerializeField]
    private GameObject _creatorPanel;

    bool lastPermitted = true;
    bool initPermission = true;

    private void Start()
    {
        AskForCameraPermission();
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)/* || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)*/)
        {
            InvokeRepeating("TestIfPermissionChanged", 1f, 1f);
        }
#elif PLATFORM_IOS
        NativeCamera.Permission currentPermission = NativeCamera.CheckPermission();
        if (currentPermission == NativeCamera.Permission.Denied || currentPermission == NativeCamera.Permission.ShouldAsk)
        {
            InvokeRepeating("TestIfPermissionChanged", 1f, 1f);
        }
#endif
    }

    public void LogImageRecognized(string img)
    {
        Debug.Log("LogImageRecognized: " + img);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("ImageRecognized", "ImgName", img);
    }

    public void OpenInfoPanel()
    {
        mainScreenObject.SetActive(false);
        infoPanelObject.SetActive(true);
    }

    public void CloseInfoPanel()
    {
        infoPanelObject.SetActive(false);
        mainScreenObject.SetActive(true);
    }

    public void OpenCreatorPanel()
    {
        infoPanelObject.SetActive(false);
        _creatorPanel.SetActive(true);
    }

    public void CloseCreatorPanel()
    {
        _creatorPanel.SetActive(false);
        infoPanelObject.SetActive(true);
        
    }

    public void ClosePopup()
    {
        popupObject.SetActive(false);
        _photoBtn.SetActive(true);
    }

    public void ShareButtonClicked()
    {
        //Debug.Log("SHAREEEE!!!");
        NativeShare ns = new NativeShare();
        ns.SetSubject("Hrad Zborov");
        ns.SetText("Ahoj, vyskúšaj si rozšírenú realitu v tejto aplikácii Zborovského hradu!" +
            "\nhttps://play.google.com/store/apps/details?id=com.tastyAir.arCastle");
        //ns.AddFile(file);
        ns.SetTitle("Hrad Zborov");
        ns.Share();
        Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventShare);
    }

    void TestIfPermissionChanged()
    {
#if PLATFORM_ANDROID
        if (!lastPermitted && Permission.HasUserAuthorizedPermission(Permission.Camera) /*&& Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)*/)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
#endif
    }

    // Start is called before the first frame update
    public void AskForCameraPermission()
    {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            lastPermitted = false;
            if (cameraWarning)
            {
                cameraWarning.SetActive(false);
            }
            Permission.RequestUserPermission(Permission.Camera);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        //if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        //{
        //    lastPermitted = false;
        //    if (cameraWarning)
        //    {
        //        cameraWarning.SetActive(false);
        //    }
        //    Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        //    //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //}
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)/* || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)*/)
        {
            if (cameraWarning)
            {
                cameraWarning.SetActive(true);
                mainScreenObject.SetActive(false);
            }
        }
        else
        {
            if (cameraWarning)
            {
                cameraWarning.SetActive(false);
                mainScreenObject.SetActive(true);
            }
        }
#elif PLATFORM_IOS && !UNITY_EDITOR
        NativeCamera.Permission currentPermission = NativeCamera.CheckPermission();

        if (currentPermission != NativeCamera.Permission.Granted)
        {
            lastPermitted = false;
            if (cameraWarning)
            {
                cameraWarning.SetActive(false);
            }

            if (!initPermission && currentPermission == NativeCamera.Permission.Denied) //pokud neni init ze Start metody (volano pres tlacitko) 
                NativeCamera.OpenSettings();
            else
                currentPermission = NativeCamera.RequestPermission();

            initPermission = false;
        }
       
        if (currentPermission != NativeCamera.Permission.Granted)
        {
            if (cameraWarning)
            {
                cameraWarning.SetActive(true);
                mainScreenObject.SetActive(false);
            }
        }
        else
        {
            if (cameraWarning)
            {
                cameraWarning.SetActive(false);
                mainScreenObject.SetActive(true);
            }
        }
#endif

    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public static void SendEmail(string htmlBody, string subject, string to, string[] attachments)
    {
        try
        {
            Debug.Log("Start of sending email...");
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            Debug.Log("Inicialized");
            message.From = new MailAddress("ondrasek2002@gmail.com", "Ondřej Hráček", System.Text.Encoding.UTF8);
            message.To.Add(new MailAddress(to));
            message.To.Add(new MailAddress("ondra.hracek@seznam.cz"));
            message.Subject = subject;
            message.IsBodyHtml = true; //to make message body as html  
            message.Body = htmlBody;

            for (int i = 0; i < attachments.Length; i++)
            {
                Attachment att = new Attachment(attachments[i]);
                message.Attachments.Add(att);
            }

            Debug.Log("Message done");

            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //for gmail host  
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("ondrasek2002@gmail.com", "jpnvmplziduvjnfw");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            Debug.Log("Smtp done, sending..");
            smtp.Send(message);
            Debug.Log("Email sent.");
        }
        catch (SmtpException e)
        {
            Debug.Log("Error!" + e);
        }
    }
}
