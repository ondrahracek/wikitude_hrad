﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraOnEnableScript : MonoBehaviour
{
    public Transform target;


    

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            Vector3 lastRot = transform.eulerAngles;
            transform.LookAt(target);
            Vector3 newRot = transform.eulerAngles;
            newRot.x = lastRot.x;
            newRot.z = lastRot.z;
            transform.eulerAngles = newRot;
        }
    }
}
