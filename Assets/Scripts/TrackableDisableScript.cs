﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackableDisableScript : MonoBehaviour
{
    public bool enableCross = false;

    TrackableDisableScript[] trackableDisables;
    public bool recognised = false;
    public GameObject myImage;
    public bool uiHolder = false;
    public GameObject disableButton;

    float lastTouchXPos = -1;
    public float rotationPerScreen = 360f;
    Vector2 lastScaleCenter = Vector2.zero;
    float lastScaleDistance = -1;
    public float scalePerScreen = 10f;

    public bool imageRecognisedB = false;

    public GameObject myUIButtons;

    public void ImageRecognised()
    {
        trackableDisables = FindObjectsOfType<TrackableDisableScript>();
        recognised = true;
        foreach (TrackableDisableScript s in trackableDisables)
        {
            if (s.recognised && s != this && enableCross)
                s.DisableImage();
            if (s.uiHolder && enableCross)
                s.EnableUIObject();
        }
        if (myImage)
            myImage.SetActive(true);
        if (myUIButtons)
            myUIButtons.SetActive(true);
    }

    public void ImageLost()
    {
        recognised = false;
        DisableImage();
    }

    public void DisableImage()
    {
        if (myImage)
        {
            myImage.SetActive(false);
            myImage.transform.localScale = Vector3.one;
        }
        recognised = false;
        foreach (TrackableDisableScript s in trackableDisables)
        {
            if (s.uiHolder)
                s.DisableUIObject();
        }
        if (myUIButtons)
            myUIButtons.SetActive(false);

        //Instantiate(this.gameObject, transform.position, transform.rotation, transform.parent);
        //Destroy(this.gameObject);
    }

    public void DisableButtonClicked()
    {
        trackableDisables = FindObjectsOfType<TrackableDisableScript>();
        foreach (TrackableDisableScript s in trackableDisables)
        {
            s.DisableImage();
        }
        if (uiHolder && disableButton)
            disableButton.SetActive(false);
    }

    public void EnableUIObject()
    {
        if (disableButton)
            disableButton.SetActive(true);
        Debug.Log("ENABLING");
    }

    public void DisableUIObject()
    {
        if (disableButton)
            disableButton.SetActive(false);
        Debug.Log("DISABLING");
    }


    // Start is called before the first frame update
    void Start()
    {
        trackableDisables = FindObjectsOfType<TrackableDisableScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (myImage && enableCross)
            myImage.SetActive(recognised);
        if (imageRecognisedB)
        {
            imageRecognisedB = false;
            if (recognised)
            {
                ImageLost();
            }
            else
            {
                ImageRecognised();
            }
            Debug.Log(trackableDisables.Length);
        }


        if (Input.touchCount >= 1 && recognised && myImage)
        {
            if (Input.touchCount == 1)
            {
                Touch t = Input.GetTouch(0);
                float nowPos = t.position.x;
                float move = -1;
                if (t.phase == TouchPhase.Began)
                {
                    lastTouchXPos = nowPos;
                }
                else if (t.phase == TouchPhase.Moved)
                {
                    move = nowPos - lastTouchXPos;
                    lastTouchXPos = nowPos;
                }
                else if (t.phase == TouchPhase.Ended || t.phase == TouchPhase.Canceled)
                {
                    lastTouchXPos = -1;
                }
                if (lastTouchXPos != -1 && move != -1 && myImage)
                {
                    move /= Screen.width;
                    move *= rotationPerScreen;
                    myImage.transform.Rotate(new Vector3(0, move, 0), Space.World);
                }
            }
            if (Input.touchCount == 2)
            {
                Touch ta = Input.GetTouch(0);
                Touch tb = Input.GetTouch(1);
                Vector2 apos = ta.position;
                Vector2 bpos = tb.position;
                float nowScaleDistance = Vector2.Distance(apos, bpos);
                //Vector2 nowScaleCenter = (apos + bpos) / 2;
                //if (lastScaleCenter != Vector2.zero)
                //{
                //    if(Vector2.Distance(lastScaleCenter, nowScaleCenter) < )
                //}
                if (lastScaleDistance != -1)
                {
                    float delta = nowScaleDistance - lastScaleDistance;
                    float changeMultiplier = 1 + delta / new Vector2(Screen.width, Screen.height).magnitude * scalePerScreen;
                    myImage.transform.localScale *= changeMultiplier;
                }
                //else
                //{
                //    myImage.transform.localScale = Vector3.one;
                //}
                lastScaleDistance = nowScaleDistance;
                //lastScaleCenter = nowScaleCenter;
            }
            else
            {
                lastScaleDistance = -1;
            }
        }
        else
        {
            lastScaleDistance = -1;
        }
    }
}
