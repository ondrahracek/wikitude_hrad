var searchData=
[
  ['physicaltargetimageheights',['PhysicalTargetImageHeights',['../class_wikitude_1_1_image_tracker.html#a72840c845116cd3e15a895ff0a67cd43',1,'Wikitude::ImageTracker']]],
  ['pixelstride',['PixelStride',['../struct_wikitude_1_1_camera_frame_plane.html#a9b6f77f96f4bc97216fa970dc451a51e',1,'Wikitude::CameraFramePlane']]],
  ['planedrawable',['PlaneDrawable',['../class_wikitude_1_1_instant_trackable.html#a8d771b6a418ad92d513d2b103b4061fb',1,'Wikitude::InstantTrackable']]],
  ['platformbase',['PlatformBase',['../class_wikitude_1_1_platform_base.html',1,'Wikitude']]],
  ['plugin',['Plugin',['../class_wikitude_1_1_plugin.html',1,'Wikitude']]],
  ['printerrorstoconsole',['PrintErrorsToConsole',['../class_wikitude_1_1_wikitude_camera.html#a3d4e70fad52c57388f09ba0650803758',1,'Wikitude::WikitudeCamera']]]
];
